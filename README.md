<!--
This script is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this script.  If not, see <https://www.gnu.org/licenses/>.
-->

# bash Cloudflare blocker
This is a bash script which blocks or unblocks the Cloudflare IPs from iptables.

This bash script requires iptables.

## The manual

| The manual                       |                             |
| -------------------------------- | --------------------------- |
| ./cloudflare-blocker.sh          | blocks the Cloudflare ips   |
| ./cloudflare-blocker.sh -block   | blocks the Cloudflare ips   |
| ./cloudflare-blocker.sh -unblock | unblocks the Cloudflare ips |
| ./cloudflare-blocker.sh -help    | shows help                  |

## The ip-lists

Beware, the Cloudflare site uses Cloudflare.

- https://www.cloudflare.com/ips-v4
- https://www.cloudflare.com/ips-v6

## License
### GPLv3
This project is **free software** and is **licensed under the GPLv3** license.  
Contributions are welcome.
